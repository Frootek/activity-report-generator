from django.contrib.auth import get_user_model
from django import forms


#check for unique email and username

User = get_user_model()

class RegisterForm(forms.Form):
    username = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'id': 'username',
                'class': 'form-control',
            })

    )
    email = forms.EmailField(
        widget=forms.TextInput(
            attrs={
                'id': 'email',
                'class': 'form-control',
            })
    )
    password = forms.CharField(
        label='Password',
        widget=forms.PasswordInput(
            attrs = {
                'class':'form-control',
                'id':'user-password'
            }
        )
    )
    password_confirm = forms.CharField(
        label = 'Confirm password',
        widget=forms.PasswordInput(
            attrs = {
                'class':'form-control',
                'id':'user-confirm-password'
            }
        )
    )

    def clean_password_confirm(self):
        password = self.cleaned_data.get('password')
        password_confirm = self.cleaned_data.get('password_confirm')
        print(password,password_confirm)

        if password and password_confirm and password == password_confirm:
            return password
        raise forms.ValidationError('Passwords are not the same')

    def clean_username(self):
        username = self.cleaned_data.get('username')
        qs = User.objects.filter(username=username)
        if qs.exists():
            raise forms.ValidationError('This is invalid username please pick another.')
        return username

    def clean_email(self):
        email = self.cleaned_data.get('email')
        qs = User.objects.filter(email=email)
        if  qs.exists():
            raise forms.ValidationError('This email is already in use.')
        return email




class LoginForm(forms.Form):
    username = forms.CharField(
        label='Username',
        widget=forms.TextInput(
            attrs={
                'id': 'username',
                'class': 'form-control',
            })
    )
    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs = {
                'class':'form-control',
                'id':'password'
            }
        )
    )


    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')



    def clean_username(self):
        username = self.cleaned_data.get('username')
        qs = User.objects.filter(username=username)

        if not qs.exists():
            raise forms.ValidationError('This is invalid user.')

        return username

