from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, get_user_model, logout
from django.urls import reverse

from .forms import  LoginForm, RegisterForm

User = get_user_model()

def register_view(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST or None)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            email = form.cleaned_data.get('email')

            try:
                user = User.objects.create_user(username,email,password)
            except:
                user = None

            if user is not None:
                login(request, user)
                return redirect(reverse('arg-index'))
            else:
                request.session['register_error']  = 1

        return render(request, 'authorization/register_form.html',{'form': form,})
    # if method is GET , create blank form
    else:
        form = RegisterForm()
    return render(request, 'authorization/register_form.html',{'form': form})




def login_view(request):
    # if method is POST validate form and re-diret user
    if request.method == 'POST':
        form = LoginForm(request.POST or None)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                if user.is_superuser:
                    return HttpResponseRedirect(reverse('admin:index'))


                return redirect(reverse('arg-index'))
            else:
                request.session['invalid_user']  = 1



def logout_view(request):
    logout(request)
    return redirect(reverse('landing-view'))



def landing_page(request):
    logout(request)
    context = {
        'login_form' : LoginForm(),
        'register_form': RegisterForm(),
    }
    return render(request, 'authorization/index.html',context)



