from django.urls import path

from . import views

urlpatterns = [
    path('', views.landing_page, name = 'landing-view'),
    path('register', views.register_view, name = 'register'),
    path('login', views.login_view, name = 'login-view'),
    path('logout', views.logout_view, name = 'logout-view'),
]
