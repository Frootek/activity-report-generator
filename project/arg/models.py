from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Grade(models.IntegerChoices):
    excellent = 5
    very_good = 4
    good = 3
    sufficient = 2
    insufficient = 1


class Organization(models.Model):
    class Meta:
        db_table = "organization"
    name = models.CharField(max_length = 50)
    user_owned = models.ForeignKey(User, on_delete=models.CASCADE)
    description = models.CharField(max_length = 200)
    creation_date = models.DateField()

    def __str__(self):
        return self.name

    def name_to_url(self):
        return self.name.replace(' ', '-')


class Group(models.Model):
    class Meta:
        db_table = "group"

    name = models.CharField(max_length = 50)
    organization_owned = models.ForeignKey(Organization, on_delete=models.CASCADE)
    description = models.CharField(max_length = 200)
    creation_date = models.DateField()

    def __str__(self):
        return self.name

    def name_to_url(self):
        return self.name.replace(' ', '-')


class Member(models.Model):
    class Meta:
        db_table = "member"

    first_name = models.CharField(max_length = 50)
    last_name = models.CharField(max_length = 50)
    description = models.CharField(max_length = 200)
    group_owned = models.ForeignKey(Group, on_delete=models.CASCADE)
    date_added = models.DateField()

    def __str__(self):
        return ('{} {}'.format(self.first_name, self.last_name))

    def member_to_url(self):
        return (self.first_name + '-' + self.last_name)

class Activity(models.Model):
    class Meta:
        db_table = "activity"
    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    description = models.CharField(max_length = 200)
    grade = models.IntegerField(choices = Grade.choices)
    date_created = models.DateField()


class Report(models.Model):
    class Meta:
        db_table = "report"

    class ReportType(models.TextChoices):
        WEEK = "Weekly report"
        MONTH = "Monthly report"
        YEAR = "Yearly report"
    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    description = models.CharField(max_length = 200)
    grade = models.IntegerField(choices = Grade.choices)
    date_created = models.DateField()
    activities = models.ManyToManyField(Activity)
    



