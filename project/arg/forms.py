from django.contrib.auth import get_user_model
from django import forms

import datetime

forbidden_names = ['edit', 'organization','new','create', 'group','member', 'activity']


#check for unique email and username

User = get_user_model()

class OrganizationCreationForm(forms.Form):
    organization_name = forms.CharField(
        label = 'Organization name',
        widget=forms.TextInput(
            attrs={
            'id': 'organization',
            'class' : 'form-control',
             'aria-describedby' : 'organizationHelp'
            })
    )
    description = forms.CharField(
        label = 'Description',
        widget=forms.TextInput(
            attrs={
                'id': 'description',
                'class': 'form-control',
                'aria-describedby' : 'descriptionHelp'

            })
    )

    def clean_organization_name(self):
        organization_name = self.cleaned_data.get('organization_name')
        print(organization_name)


        if not all(x.isalpha() or x.isspace() for x in organization_name):
            raise forms.ValidationError('Organization name can only contain leter and spaces')

        if organization_name.lower() in forbidden_names:
            raise forms.ValidationError('Organization name is in our forbidden names list please pick another one.')

        return  organization_name

    def clean_description(self):
        description = self.cleaned_data.get('description')
        if not all(x.isalpha() or x.isspace() for x in description):
            raise forms.ValidationError('Description can only contain leter and spaces')
        return description

class GroupCreationForm(forms.Form):
    name = forms.CharField(
        label = 'Group name',
        widget=forms.TextInput(
            attrs={
                'id': 'organization',
                'class': 'form-control',
            })
    )
    description = forms.CharField(
        label = 'Description',
        widget=forms.TextInput(
            attrs={
                'id': 'description',
                'class': 'form-control',

            })
    )

    def clean_group_name(self):
        group_name = self.cleaned_data.get('name')
        print(group_name)

        if not all(x.isalpha() or x.isspace() for x in group_name):
            raise forms.ValidationError('Organization name can only contain leter and spaces')

        if group_name.lower() in forbidden_names:
            raise forms.ValidationError('Organization name is in our forbidden names list please pick another one.')
        return  group_name

    def clean_description(self):
        description = self.cleaned_data.get('description')
        if not all(x.isalpha() or x.isspace() or x.isdigit() for x in description):
            raise forms.ValidationError('Description can only contain leter and spaces')

        return description

class MemberCreationForm(forms.Form):
    first_name = forms.CharField(
        label = 'First name',
        widget = forms.TextInput(
            attrs={
                'id': 'first_name',
                'class': 'form-control',
            })
    )
    last_name = forms.CharField(
        label = 'Last name',
        widget=forms.TextInput(
            attrs={
                'id': 'last_name',
                'class': 'form-control',
            })
    )
    description = forms.CharField(
        label = 'Description',
        widget=forms.TextInput(
            attrs={
                'id': 'description',
                'class': 'form-control',
            })
    )

    def clean_first_name(self):
        first_name = self.cleaned_data.get('first_name')
        print(first_name)

        if not all(x.isalpha() or x.isspace() for x in first_name):
            raise forms.ValidationError('First name can only contain leter and spaces')

        if first_name.lower() in forbidden_names:
            raise forms.ValidationError('First name is in our forbidden names list please pick another one.')
        return  first_name

    def clean_last_name(self):
        last_name = self.cleaned_data.get('last_name')
        print(last_name)

        if not all(x.isalpha() or x.isspace() for x in last_name):
            raise forms.ValidationError('Last name can only contain leter and spaces')

        if last_name.lower() in forbidden_names:
            raise forms.ValidationError('Last name is in our forbidden names list please pick another one.')
        return  last_name

    def clean_description(self):
        description = self.cleaned_data.get('description')
        if not all(x.isalpha() or x.isspace() or x.isdigit() for x in description):
            raise forms.ValidationError('Description can only contain leter and spaces and digits.')

        return description


class ActivityCreationForm(forms.Form):
    GRADE_CHOICES = (
        ("1", "1"),
        ("2", "2"),
        ("3", "3"),
        ("4", "4"),
        ("5", "5"),
        )
    description = forms.CharField(
        label = 'Description',
        widget = forms.TextInput(
            attrs={
                'id': 'description',
                'class': 'form-control',

            })
    )
    grade = forms.MultipleChoiceField(
        label = 'Grade',
        choices = GRADE_CHOICES,
        widget=forms.Select(
            attrs={
                'id': 'grade',
                'class': 'form-control',
            })
    )

    date = forms.DateField(
        label = 'Date',
        initial=datetime.date.today,
        widget = forms.DateInput(
            attrs={
                'id': 'date',
                'class': 'form-control',
                'type': 'date',
            })
        )

    def clean_description(self):
        extra_allowed_chars = ['.', ',', '!', '?', ':']
        description = self.cleaned_data.get('description')
        if not all(x.isalpha() or x.isspace() or x.isdigit() or x in extra_allowed_chars for x in description):
            print('tu sam')
            raise forms.ValidationError('Description can only contain leter, spaces, digits  and basic punctuation.')

        return description

    def clean_grade(self):
        grade = self.cleaned_data.get('grade')
        return grade








