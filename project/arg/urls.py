from django.urls import path, re_path

from .views import *

urlpatterns = [
    path('', index, name='arg-index'),
    path('edit/organization/create', create_organization_view,name='create-organization'),
    path('<str:organization_url>/edit/group/create',create_organization_group_view, name='create-group'),
    path('<str:organization_url>/<str:group_url>/edit/member/create',create_member_view,name='create-member'),
    path('<str:organization_url>', organization_view, name='organization-view'),
    path('<str:organization_url>/<str:group_url>', group_view, name='group-view'),
    path('<str:organization_url>/<str:group_url>/<str:member_url>/edit/activities/create', create_activity, name='activity-create'),

    #DELETE PATHS
    path('<str:organization_url>/<str:group_url>/<str:member_url>/edit/activities/delete/<int:activity_id>',
         delete_member_activity, name='activity-delete'),

    path('<str:organization_url>/<str:group_url>/edit/member/delete/<int:member_id>',
      delete_member, name='member-delete'),

    path('<str:organization_url>/edit/group/delete/<int:group_id>',
         delete_group, name='group-delete'),

    path('edit/organization/delete/<int:organization_id>',
         delete_organization, name='group-delete'),

    path('<str:organization_url>/<str:group_url>/<str:member_url>', member_view, name='member-view'),
    path('tryme', tryme, name='try')
]