from django.http import HttpResponse, Http404
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model

from django.shortcuts import redirect, render
from django.urls import reverse

from ..forms import  OrganizationCreationForm, GroupCreationForm, MemberCreationForm, ActivityCreationForm
from ..models import  Organization, Group, Member, Activity

import datetime

User = get_user_model()

@login_required(login_url='')
def index(request, filled_form = None):
    request_user = User.objects.filter(username = request.user).first()
    users_organizations_list = Organization.objects.filter(user_owned = request_user.id)

    if filled_form is not None:
        form = filled_form
    else:
        form = OrganizationCreationForm()
    context  = {
        'organizations_list' : users_organizations_list,
        'user' :  request_user,
        'form' : form,
    }
    return render(request, 'arg/index.html', context)


@login_required(login_url='')
def organization_view(request, organization_url, filled_form = None):
    #Validate url
    if(' ' in  organization_url):
        raise Http404('Invalid url')

    organization_name = organization_url.replace('-', ' ')
    print(organization_name + '- ovo je ime organizacije')

    try:
        request_user = User.objects.get(username=request.user)
        organization = Organization.objects.get(user_owned = request_user.id, name=organization_name)
    except:
        raise Http404('Organization {} for user {} does not exist!'.format(organization_name, request_user.username))

    groups = Group.objects.filter(organization_owned_id = organization.id)

    if filled_form is None:
        form = GroupCreationForm()
    else:
        form = filled_form

    context = {
        'organization' : organization,
        'organization_groups' : groups,
        'form' : form,
    }
    return render(request, 'arg/organization_groups.html', context)


@login_required(login_url='')
def group_view(request,organization_url, group_url, filled_form = None):
    #Validate url
    if(' ' in  group_url or ' ' in organization_url):
        raise Http404('Invalid url')

    organization_name = organization_url.replace('-', ' ')
    group_name = group_url.replace('-', ' ')
    print(group_name)

    try:
        request_user = User.objects.get(username=request.user)
        organization = Organization.objects.get(user_owned = request_user.id, name=organization_name)
        group = Group.objects.get(organization_owned_id = organization.id, name = group_name)
    except:
        raise Http404('Organization {} for user {} does not exist!'.format(organization_name, request_user.username))

    members = Member.objects.filter(group_owned_id = group.id)

    if filled_form is None:
        form = MemberCreationForm()
    else:
        form = filled_form

    context = {
        'organization' : organization,
        'group' : group,
        'group_members' : members,
        'form' : form
    }
    return render(request, 'arg/group_members.html', context)


@login_required(login_url='')
def member_view(request,organization_url, group_url, member_url, filled_form= None):
    #Validate url
    if(' ' in  group_url or ' ' in organization_url or ' ' in member_url):
        raise Http404('Invalid url')

    organization_name = organization_url.replace('-', ' ')
    group_name = group_url.replace('-', ' ')
    member_full_name = member_url.replace('-', ' ')
    member_full_name = member_full_name.split()
    first_name = member_full_name[0]
    last_name = member_full_name[1]

    try:
        request_user = User.objects.get(username=request.user)
        organization = Organization.objects.get(user_owned = request_user.id, name=organization_name)
        group = Group.objects.get(organization_owned_id = organization.id, name = group_name)
        member = Member.objects.get(group_owned_id = group.id, first_name = first_name,last_name = last_name)
    except:
        raise Http404('Somethin went wrong!')

    activities = Activity.objects.filter(member_id = member.id)

    data = {
        1:0,
        2:0,
        3:0,
        4:0,
        5:0
    }
    for activity in activities:
        a = data.get(int(activity.grade),0)
        a += 1
        data[int(activity.grade)] = a

    print(data)


    print(data)
    if filled_form is None:
        form = ActivityCreationForm()
    else:
        form = filled_form

    bar_chart = []
    for i in range(5):
        bar_chart.append(data[i+1])



    context = {
        'member' : member,
        'activities' : activities,
        'organization' : organization,
        'group' : group,
        'form' : form,
        'pie_chart_data' : data,
        'bar_chart': bar_chart
    }
    return render(request, 'arg/member.html', context)

@login_required(login_url='')
def create_organization_view(request):
    if request.method == 'POST':
        print(request.POST)
        form = OrganizationCreationForm(request.POST or None)
        print(form)
        if form.is_valid():
            organization_name = form.cleaned_data.get('organization_name')
            description = form.cleaned_data.get('description')
            #print(request.user)
            #print(request.user.id)

            #print(organization_name)
            #print(description)
            ##check if that organization already exist
            ##if does rais http404

            check_organization = Organization.objects.filter(
                name = organization_name,
                user_owned_id = request.user.id
            )

            if(len(check_organization)!= 0):
                raise Http404('You already created that organization')

            user = User.objects.get(username=request.user)
            today = datetime.date.today()

            try:
                new_organization = Organization(
                    name = organization_name,
                    user_owned = user,
                    description = description,
                    creation_date = today
                )
                new_organization.save()
            except:
                raise Http404('Something went wrong...')

            return redirect(reverse('arg-index'))

    else:
        form = OrganizationCreationForm()

    return index(request, filled_form=form)

@login_required(login_url='')
def create_activity(request, organization_url, group_url, member_url):
    if request.method == 'POST':

        form = ActivityCreationForm(request.POST or None)

        description = form.data.get('description')
        grade = form.data.get('grade')
        date = form.data.get('date')


        organization_name = organization_url.replace('-', ' ')
        group_name = group_url.replace('-', ' ')
        member_full_name  =member_url.replace('-', ' ')

        first_name, last_name = member_full_name.split()

        try:
            organization = Organization.objects.get(
                name=organization_name,
                user_owned_id = request.user.id
            )
        except:
            raise Http404('Organization does not exist...')

        try:
            group = Group.objects.get(
                name = group_name,
                organization_owned = organization.id
            )
        except:
            raise Http404('Group does not exist...')

        try:
            member = Member.objects.get(
                first_name = first_name,
                last_name = last_name,
                group_owned = group
            )
        except:
            raise Http404('Member does not exist...')

        try:
            new_activity = Activity(
                description = description,
                grade = grade[0],
                date_created = date,
                member_id = member.id
            )
            print(new_activity)
            new_activity.save()
        except Exception:
            import traceback
            traceback.print_exc()
            raise Http404('Something went wrong...')

        return redirect(reverse('member-view',
                                args = [organization_url,group_url, member_url]))



@login_required(login_url='')
def create_organization_group_view(request, organization_url):
    if request.method == 'POST':
        form = GroupCreationForm(request.POST or None)
        if form.is_valid():
            name = form.cleaned_data.get('name')
            description = form.cleaned_data.get('description')

            organization_name = organization_url.replace('-', ' ')

            try:
                selected_organization = Organization.objects.get(
                    name=organization_name,
                    user_owned_id = request.user.id
                )
            except:
                raise Http404('Organization does not exist...')


            check_group = Group.objects.filter(
                name = name,
                organization_owned_id = selected_organization.id
            )

            if(len(check_group)!= 0):
                raise Http404('You already created that group for this organization')

            today = datetime.date.today()

            try:
                new_group = Group(
                    name = name,
                    organization_owned_id = selected_organization.id,
                    description = description,
                    creation_date = today
                )
                new_group.save()
            except:
                raise Http404('Something went wrong...')

            return redirect(reverse('organization-view', args = [organization_url]))

    else:
        form = GroupCreationForm()

    return organization_view(request, organization_url,filled_form= form)



@login_required(login_url='')
def create_member_view(request, organization_url, group_url):
    if request.method == 'POST':
        print('prvi group_url je ovaj::' + group_url)
        form = MemberCreationForm(request.POST or None)
        if form.is_valid():
            first_name = form.cleaned_data.get('first_name')
            last_name = form.cleaned_data.get('last_name')
            description = form.cleaned_data.get('description')

            organization_name = organization_url.replace('-', ' ')
            group_name = group_url.replace('-', ' ')

            try:
                selected_organization = Organization.objects.get(
                    name=organization_name,
                    user_owned_id = request.user.id
                )
            except:
                raise Http404('Organization does not exist...')

            try:
                selected_group = Group.objects.get(
                    name = group_name,
                    organization_owned=selected_organization,
                )
            except:
                raise Http404('Group does not exist...')



            check_member = Member.objects.filter(
                first_name = first_name,
                last_name = last_name,
                group_owned = selected_group
            )

            if(len(check_member)!= 0):
                raise Http404('You already created that member for this group')

            today = datetime.date.today()

            try:
                new_member = Member(
                    first_name = first_name,
                    last_name = last_name,
                    group_owned = selected_group,
                    description = description,
                    date_added = today
                )
                new_member.save()
            except Exception:
                import traceback
                traceback.print_exc()
                raise Http404('Something went wrong...')
            print(group_url)
            return redirect(reverse('group-view', args = [organization_url,group_url]))

    else:
        form = MemberCreationForm()

    return group_view(request,organization_url,group_url,form)


def tryme(request):
    return HttpResponse("probaj me")

