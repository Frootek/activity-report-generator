from django.http import Http404
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model

from django.shortcuts import redirect
from django.urls import reverse

from ..models import  Organization, Group, Member, Activity

User = get_user_model()

@login_required(login_url='')
def delete_member_activity(request, organization_url, group_url, member_url, activity_id):
    organization_name = organization_url.replace('-', ' ')
    group_name = group_url.replace('-', ' ')
    member_full_name = member_url.replace('-', ' ')

    try:
        request_user = User.objects.get(username=request.user)
        organization = Organization.objects.get(user_owned = request_user.id, name=organization_name)
        group = Group.objects.get(organization_owned_id = organization.id, name = group_name)
        member_full_name = member_full_name.split()
        first_name = member_full_name[0]
        last_name = member_full_name[1]
        member = Member.objects.get(group_owned_id = group.id, first_name = first_name,last_name = last_name)
        activities = Activity.objects.filter(member_id = member.id)

        activity_delete = activities.filter(id=activity_id)

    except:
        raise Http404('Somethin went wrong!')

    activity_delete.delete()

    return redirect(reverse('member-view', args = [organization_url,group_url,member_url]))

@login_required(login_url='')
def delete_member(request, organization_url, group_url, member_id):
    organization_name = organization_url.replace('-', ' ')
    group_name = group_url.replace('-', ' ')

    try:
        request_user = User.objects.get(username=request.user)
        organization = Organization.objects.get(user_owned = request_user.id, name=organization_name)
        group = Group.objects.get(organization_owned_id = organization.id, name = group_name)
        member = Member.objects.get(group_owned_id = group.id, id=member_id)

    except:
        raise Http404('Somethin went wrong!')

    member.delete()

    return redirect(reverse('group-view', args = [organization_url,group_url]))

@login_required(login_url='')
def delete_group(request, organization_url, group_id):
    organization_name = organization_url.replace('-', ' ')

    try:
        request_user = User.objects.get(username=request.user)
        organization = Organization.objects.get(user_owned = request_user.id, name=organization_name)
        group = Group.objects.get(organization_owned_id = organization.id, id = group_id)

    except:
        raise Http404('Somethin went wrong!')

    group.delete()

    return redirect(reverse('organization-view', args = [organization_url]))

@login_required(login_url='')
def delete_organization(request, organization_id):
    try:
        request_user = User.objects.get(username=request.user)
        organization = Organization.objects.get(user_owned = request_user.id, id=organization_id)

    except:
        raise Http404('Somethin went wrong!')

    organization.delete()

    return redirect(reverse('arg-index'))
